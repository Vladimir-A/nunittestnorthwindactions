﻿using OpenQA.Selenium;

namespace NUnitTestNorthwindActions
{
    class HomePage
    {
        public HomePage(IWebDriver d)
        {
            driver = d;
        }

        private IWebDriver driver;
        public IWebElement AllProducts
        {
            get { return driver.FindElement(By.LinkText("All Products")); }
        }
        public IWebElement CreateNew
        {
            get { return driver.FindElement(By.LinkText("Create new")); }
        }
        public IWebElement ProductName
        {
            get { return driver.FindElement(By.Id("ProductName")); }
        }
        public IWebElement CategoryID
        {
            get { return driver.FindElement(By.Id("CategoryId")); }
        }
        public IWebElement SupplierID
        {
            get { return driver.FindElement(By.Id("SupplierId")); }
        }
        public IWebElement UnitPrice
        {
            get { return driver.FindElement(By.Id("UnitPrice")); }
        }
        public IWebElement QuantityPerUnit
        {
            get { return driver.FindElement(By.Id("QuantityPerUnit")); }
        }
        public IWebElement UnitsInStock
        {
            get { return driver.FindElement(By.Id("UnitsInStock")); }
        }
        public IWebElement UnitsOnOrder
        {
            get { return driver.FindElement(By.Id("UnitsOnOrder")); }
        }
        public IWebElement ReorderLevel
        {
            get { return driver.FindElement(By.Id("ReorderLevel")); }
        }
        public IWebElement Confirm
        {
            get { return driver.FindElement(By.CssSelector(".btn")); }
        }
        public IWebElement Home
        {
            get { return driver.FindElement(By.LinkText("Home")); }
        }
        public IWebElement logoutField
        {
            get { return driver.FindElement(By.LinkText("Logout")); }
        }
        public IWebElement TestProduct
        {
            get { return driver.FindElement(By.LinkText("testproduct")); }
        }
        public IWebElement RemoveProduct
        {
            get { return driver.FindElement(By.XPath("//tr[79]//td[12]//a[1]")); }
        }

    }
}
