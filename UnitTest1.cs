using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace NUnitTestNorthwindActions
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();
            driver.Url = "http://localhost:5000";
        }

        [Test]
        public void t01_TestAddNewProduct()
        {
            driver = new LoginPage(driver).Login("user", "user");
            Assert.IsTrue(new LoginPage(driver).isLoginSuccessfull(), "Login failed");

            driver = new Product(driver).Add("testproduct", "10.0000", "10 boxes x 20 bags", "5", "2", "3");
            Assert.IsTrue(new Product(driver).isAddSuccessfull("testproduct"), "Add new product failed");

            driver.Close();
        }

        [Test]
        public void t02_TestNewProductAndDelete()
        {
            driver = new LoginPage(driver).Login("user","user");

            Assert.IsTrue(new LoginPage(driver).isLoginSuccessfull(), "Login failed");

            Product Pr = new Product(driver);
            Pr.isExist("testproduct", "10.0000", "10 boxes x 20 bags", "5", "2", "3");

            Assert.IsTrue(new Product(driver).isNameSuccessfull(), "Product not exist");
            Assert.IsTrue(new LoginPage(driver).isLogoutSuccessfull(), "Logout failed");

            driver.Close();
        }

        [Test]
        public void t03_DeleteNew()
        {
            driver = new LoginPage(driver).Login("user", "user");

            Assert.IsTrue(new LoginPage(driver).isLoginSuccessfull(), "Login failed");
            Product Pr = new Product(driver);
            Pr.Delete();
            Assert.IsTrue(new Product(driver).isDeleteSuccessfull(), "Remove failed");

            driver.Close();
        }

        private IWebDriver driver;
    }
}