﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace NUnitTestNorthwindActions
{
    class Product
    {
        public Product(IWebDriver d)
        { 
            driver = d;
            mainpage = new HomePage(driver);
        }

        public IWebDriver Add(string Product, string Price, string Quantity, string Stock, string Order, string Level)
        {
            mainpage.AllProducts.Click();
            mainpage.CreateNew.Click();
            mainpage.ProductName.SendKeys(Product);
            mainpage.CategoryID.FindElement(By.XPath("//option[. = 'Beverages']")).Click();
            mainpage.SupplierID.FindElement(By.XPath("//option[. = 'Exotic Liquids']")).Click();
            mainpage.UnitPrice.SendKeys(Price);
            mainpage.QuantityPerUnit.SendKeys(Quantity);
            mainpage.UnitsInStock.SendKeys(Stock);
            mainpage.UnitsOnOrder.SendKeys(Order);
            mainpage.ReorderLevel.SendKeys(Level);
            mainpage.Confirm.Click();
            return driver;
        }
        public IWebDriver Delete()
        {
            mainpage.AllProducts.Click();
            mainpage.RemoveProduct.Click();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            return driver;
        }

        public IWebDriver isExist(string Product, string Price, string Quantity, string Stock, string Order, string Level)
        {
            mainpage.AllProducts.Click();
            mainpage.TestProduct.Click();
            Assert.AreEqual(mainpage.ProductName.GetAttribute("value"), Product);
            Assert.AreEqual(mainpage.CategoryID.FindElement(By.XPath("//option[. = 'Seafood']")).GetAttribute("text"), "Seafood");
            Assert.AreEqual(mainpage.SupplierID.FindElement(By.XPath("//option[. = 'Leka Trading']")).GetAttribute("text"), "Leka Trading");
            Assert.AreEqual(mainpage.UnitPrice.GetAttribute("value"), Price);
            Assert.AreEqual(mainpage.QuantityPerUnit.GetAttribute("value"), Quantity);
            Assert.AreEqual(mainpage.UnitsInStock.GetAttribute("value"), Stock);
            Assert.AreEqual(mainpage.UnitsOnOrder.GetAttribute("value"), Order);
            Assert.AreEqual(mainpage.ReorderLevel.GetAttribute("value"), Level);
            mainpage.Home.Click();
            mainpage.logoutField.Click();
            return driver;
        }

        public bool isAddSuccessfull(string name)
        {
            bool isAddSuccessfull = false;
            try
            {
                IWebElement add = driver.FindElement(By.LinkText(name));
                isAddSuccessfull = add.Displayed;
            }
            catch (NoSuchElementException)
            {
                isAddSuccessfull = false;
            }
            return isAddSuccessfull;
        }
        public bool isNameSuccessfull()
        {
            bool isNameSuccessfull = false;
            try
            {
                IWebElement add = driver.FindElement(By.LinkText("All Products"));
                isNameSuccessfull = add.Displayed;
            }
            catch (NoSuchElementException)
            {
                isNameSuccessfull = false;
            }
            return isNameSuccessfull;
        }

        public bool isDeleteSuccessfull()
        {
            bool isDeleteSuccessfull = false;
            try
            {
                IWebElement delete = driver.FindElement(By.LinkText("testproduct"));
                isDeleteSuccessfull = delete.Displayed;
            }
            catch (NoSuchElementException)
            {
                isDeleteSuccessfull = true;
            }
            return isDeleteSuccessfull;
        }

        private IWebDriver driver;
        private HomePage mainpage;
    }
}
