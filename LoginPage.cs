﻿using OpenQA.Selenium;
using System;

namespace NUnitTestNorthwindActions
{
    class LoginPage
    {
        public LoginPage(IWebDriver d)
        {
            driver = d;
        }

        public IWebDriver Login(String login, String passwod)
        {
            loginField = driver.FindElement(By.Id("Name"));
            passwordField = driver.FindElement(By.Id("Password"));
            loginField.SendKeys(login);
            passwordField.SendKeys(passwod);
            passwordField.Submit();

            return driver;
        }

        public bool isLoginSuccessfull()
        {
            bool isLoginSuccessfull = false;
            try
            {
                IWebElement homePageLabel = driver.FindElement(By.XPath(".//*[text()='Home page']"));
                isLoginSuccessfull = homePageLabel.Displayed;
            }
            catch (NoSuchElementException)
            {
                isLoginSuccessfull = false;
            }
            return isLoginSuccessfull;
        }

        public bool isLogoutSuccessfull()
        {
            bool isLogoutSuccessfull = false;
            try
            {
                IWebElement logoimg = driver.FindElement(By.XPath(".//*[text()='Login']"));
                isLogoutSuccessfull = logoimg.Displayed;
            }
            catch (NoSuchElementException)
            {
                isLogoutSuccessfull = false;
            }
            return isLogoutSuccessfull;
        }

        private IWebDriver driver;
        private IWebElement loginField;
        private IWebElement passwordField;
    }
}
